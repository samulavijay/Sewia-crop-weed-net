import torch 
import numpy as np 
import seg_pytorch_models.segmentation_models_pytorch as smp 
from dataloader.dataset import Dataset 
from torch.utils.data import DataLoader
import utils 
import pytorch_lightning as pl
from colorizer import Colorizer
from denormalize import DeNormalize
import argparse

class CropWeedSewia(pl.LightningModule):
    def __init__(self, arch, encoder_name, in_channels, out_classes,lr=1e-3, **kwargs):
        super().__init__()
        self.model = smp.create_model(arch, encoder_name=encoder_name, in_channels=in_channels, classes=out_classes, **kwargs)
        self.lr = lr
        # preprocessing parameters for image 
        params = smp.encoders.get_preprocessing_params(encoder_name)
        self.register_buffer("std", torch.tensor(params["std"]).view(1, 3, 1, 1))
        self.register_buffer("mean", torch.tensor(params["mean"]).view(1, 3, 1, 1))

        # For image segmentation dice loss could be the best first choice
        self.loss_fn = smp.losses.FocalLoss(mode="multiclass",gamma=2)

    
    def forward(self, image):
        
        return self.model(image)
    
    def shared_step(self, batch, subset):
        image = batch[0]
        
        # Shape of the image should be (batch_size, num_channels, height, width)
        assert image.ndim ==4

        # check that image dimensions are divisible by 32,
        # encoder and decoder connected by skip connections and usually encoder have 5 stages of downsampling by factor 2 (2^5 = 32); e.g. if we have iamge with shape 65x65 we will have following shapes of features in encoder and decoder: 84, 42, 32, 10, 5 -> 5, 10, 20, 40, 80 and we will get an error trying to concat these features
        h, w = image.shape[2:]
        assert h % 32 ==0 and w % 32 == 0

        mask = batch[1]

        # Shape of the mask should be [batch_size, num_classes, height, width]
        # for binary segmentation num_classes = 1
        assert mask.ndim == 3

        pred_mask = self.forward(image)
        # Predicted mask contains logits, and loss_fn param `from_logits` is set to True
        loss = self.loss_fn(pred_mask, mask)

        # Lets compute metrics for some threshold
        # first convert mask values to probabilities, then 
        # apply thresholding
        pred_mask = pred_mask.argmax(dim=1)
        # print(pred_mask.shape)

        # We will compute IoU metric by two ways
        #   1. dataset-wise
        #   2. image-wise
        # but for now we just compute true positive, false positive, false negative and
        # true negative 'pixels' for each image and class
        # these values will be aggregated in the end of an epoch
        tp, fp, fn, tn = smp.metrics.get_stats(pred_mask.long(), mask.long(), mode="multiclass",num_classes=3)
        return {
            "loss": loss,
            "tp": tp,
            "fp": fp,
            "fn": fn,
            "tn": tn,
        }

    def shared_epoch_end(self, outputs, subset):
        # aggregate step metics
        tp = torch.cat([x["tp"] for x in outputs])
        fp = torch.cat([x["fp"] for x in outputs])
        fn = torch.cat([x["fn"] for x in outputs])
        tn = torch.cat([x["tn"] for x in outputs])
        loss = torch.stack([x["loss"] for x in outputs])
        # per image IoU means that we first calculate IoU score for each image 
        # and then compute mean over these scores
        per_image_iou = smp.metrics.iou_score(tp, fp, fn, tn, reduction="micro-imagewise")
        
        # dataset IoU means that we aggregate intersection and union over whole dataset
        # and then compute IoU score. The difference between dataset_iou and per_image_iou scores
        # in this particular case will not be much, however for dataset 
        # with "empty" images (images without target class) a large gap could be observed. 
        # Empty images influence a lot on per_image_iou and much less on dataset_iou.
        dataset_iou = smp.metrics.iou_score(tp, fp, fn, tn, reduction="micro")
        metrics = {
            f"{subset}_per_image_iou": per_image_iou,
            f"{subset}_dataset_iou": dataset_iou,
            f"{subset}_loss": loss,
        }
        
        self.log_dict(metrics, prog_bar=True)
    def predict_step(self, batch, batch_idx):
        image = batch[0]
        mask = batch[1]
        pred_mask = self.forward(image)
        pred_mask = pred_mask.argmax(dim=1)
        line = np.ones((640,5,1,3))*255
        color_map = {0: [0,0,0], 1: [0,255,0], 2: [0,0,255]}
        colorizer = Colorizer(color_map)
        masks_colored = colorizer.do(mask.transpose(2,0))
        
        pred_masks_colored = colorizer.do(pred_mask.transpose(2,0))
        cat_images = np.concatenate((masks_colored,line,pred_masks_colored),axis=1)
        return cat_images

    def training_step(self, batch, batch_idx):
        return self.shared_step(batch, "train")            

    def training_epoch_end(self, outputs):
        return self.shared_epoch_end(outputs, "train")

    def validation_step(self, batch, batch_idx):
        return self.shared_step(batch, "valid")

    def validation_epoch_end(self, outputs):
        return self.shared_epoch_end(outputs, "valid")

    def test_step(self, batch, batch_idx):
        return self.shared_step(batch, "test")  

    def test_epoch_end(self, outputs):
        return self.shared_epoch_end(outputs, "test")
        

    def configure_optimizers(self):
        opt = torch.optim.Adam(self.model.parameters(), lr=self.lr)
        sch = torch.optim.lr_scheduler.CosineAnnealingLR(opt, T_max=10)
        return [opt],[sch]

    

if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument("--dataset_path","-d", type=str, help="path where the dataset is stored")
    parser.add_argument("--batch_size","-bs", type= int, default =16, help = "size of the batches")
    parser.add_argument("--lr","-lr", type=float, default = 0.001, help= "adam:learning rate")
    parser.add_argument("--encoder","-en", type=str, default = "efficientnet-b4", help= "encoder architecture to use eg: resnet, efficientnet")
    parser.add_argument("--decoder","-de", type=str, default = "UNet", help= "decoder architecture to use eg: FPN, UNet, PSPNet")
    parser.add_argument("--input_channels","-input", type= int, default =3, help = "Input channels of images")
    # parser.add_argument("--checkpoints","-ck", type=str, default = "./checkpoints/", help= "checkpoint directory path, Path should be checkpoints_encoder_decoder")
    args = parser.parse_args()

    ENCODER = "se_resnext50_32x4d"
    ENCODER_WEIGHTS = "imagenet"
    CLASSES = ["soil","weed","crop"]
    ROOT = args.dataset_path

    preprocessing_fn = smp.encoders.get_preprocessing_fn(ENCODER, ENCODER_WEIGHTS)

    train_dataset = Dataset( root=ROOT, subset="train",  crop_h = 640, crop_w = 480,augmentation= utils.get_augmentation("train"), preprocessing = utils.get_preprocessing(preprocessing_fn))

    valid_dataset = Dataset( root=ROOT, subset="valid",  crop_h = 640, crop_w = 480,augmentation= utils.get_augmentation("valid"), preprocessing = utils.get_preprocessing(preprocessing_fn))

    train_loader = DataLoader(train_dataset, batch_size=16, shuffle=True, num_workers=12)
    valid_loader = DataLoader(valid_dataset, batch_size=1, shuffle=False, num_workers=4)
    model = CropWeedSewia(args.decoder, args.encoder, in_channels=args.input_channels, out_classes=3)
    checkpoint_callback = pl.callbacks.ModelCheckpoint(
        dirpath=  "checkpoints_"+args.encoder+"_"+args.decoder,
        filename = "syn_plants-{epoch:02d}-{val_loss:.2f}",
        verbose = True,
        monitor ="valid_loss",
        
    )
    trainer = pl.Trainer(gpus=1,max_epochs=100,callbacks=[checkpoint_callback])

    trainer.fit(model,train_dataloader=train_loader,val_dataloaders=valid_loader)