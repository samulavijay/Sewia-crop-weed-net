FROM nvidia/cuda:11.1-cudnn8-devel-ubuntu18.04

CMD ["bash"]

# ENVIRONMENT STUFF FOR CUDA


ENV LD_LIBRARAY_PATH /usr/local/cuda/lib64:$LD_LIBRARAY_PATH
ENV LD_LIBRARY_PATH $LD_LIBRARY_PATH:/usr/local/cuda/lib64
ENV LD_LIBRARY_PATH $LD_LIBRARY_PATH:/usr/local/lib
ENV PATH=/usr/local/cuda/bin:$PATH
ENV CUDA_ROOT /usr/local/cuda

# to install tk-inter 
ARG DEBIAN_FRONTEND=noninteractive
ENV TZ=Europe/Berlin

RUN rm /etc/apt/sources.list.d/cuda.list
RUN rm /etc/apt/sources.list.d/nvidia-ml.list
RUN apt-get update && apt-get install -yqq  build-essential ninja-build \
  python3-dev python3-pip tig apt-utils curl git cmake unzip autoconf autogen \
  libtool mlocate zlib1g-dev python python3-numpy python3-wheel wget \
  software-properties-common openjdk-8-jdk libpng-dev  \
  libxft-dev vim meld sudo ffmpeg python3-pip libboost-all-dev \
  libyaml-cpp-dev libjpeg-dev zlib1g-dev python3-tk -y && updatedb 



RUN wget -qO - https://apt.kitware.com/keys/kitware-archive-latest.asc | apt-key add - && \
    apt-add-repository 'deb https://apt.kitware.com/ubuntu/ bionic main' && \
    apt update -y && \
    apt install cmake --upgrade -y

# clean the cache
RUN apt update && \
  apt autoremove --purge -y && \
  apt clean -y

RUN rm -rf /var/lib/apt/lists/*

# to use nvidia driver from within
LABEL com.nvidia.volumes.needed="nvidia_driver"

# this is to be able to use graphics from the container
# Replace 1000 with your user / group id (if needed)
RUN export uid=1000 gid=1000 && \
  mkdir -p /home/sewia && \
  mkdir -p /etc/sudoers.d && \
  echo "sewia:x:${uid}:${gid}:Sewia,,,:/home/sewia:/bin/bash" >> /etc/passwd && \
  echo "sewia:x:${uid}:" >> /etc/group && \
  echo "sewia ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers.d/sewia && \
  chmod 0440 /etc/sudoers.d/sewia && \
  chown ${uid}:${gid} -R /home/sewia && \
  adduser sewia sudo

ENV HOME /home/sewia
WORKDIR $HOME



RUN chown -R sewia:sewia $HOME/
RUN chmod 755 $HOME/

RUN cd /home/sewia/ && \
    git clone --recurse-submodules https://github.com/vijaysamula/Sewia-crop-weed-net.git && \
    cd Sewia-crop-weed-net/seg_pytorch_models/ && \
    pip3 install -r requirements.txt && \
    python3 setup.py install && \
    cd .. && \
    pip3 install --upgrade pip && \
    pip3 install -r requirements.txt