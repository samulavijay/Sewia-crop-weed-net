import os 
import utils 
from dataloader.dataset import Dataset
import argparse
from train import CropWeedSewia
import torch
import numpy as np
import pytorch_lightning as pl

def visualize_labels(dataset_path,subset,device,model=None):
    if subset=="train":
        augmented_dataset = Dataset( root=dataset_path, subset="train",crop_h = 640, crop_w = 480,augmentation= utils.get_augmentation("train"), preprocessing = None)

        for i in range(3):
            image, mask = augmented_dataset[1]
            mask = utils.color_conversion_labels(mask)
            utils.visualize(image=image, mask=mask)
    elif subset=="test":
        test_dataset = Dataset( root=dataset_path, subset="test", crop_h = 640, crop_w = 480,augmentation= utils.get_augmentation("test"),preprocessing = None)
        test_dataset_vis = Dataset( root=dataset_path, subset="test", crop_h = 640, crop_w = 480)
        for i in range(5):
            n = np.random.choice(len(test_dataset))
            
            image_vis = test_dataset_vis[n][0].astype('uint8')
            image, gt_mask = test_dataset[n]
            
            gt_mask = gt_mask.squeeze()
            
            x_tensor = torch.from_numpy(image).to(device).unsqueeze(0)
            x_tensor = x_tensor.permute(0,3,1,2).type(torch.FloatTensor)
            # predictions= trainer.predict(model,dataloaders=test_dataloader)
            pr_mask = model.forward(x_tensor)
            pr_mask = (pr_mask.squeeze().detach().cpu().numpy().round())
            pr_mask = pr_mask.transpose(1,2,0)
            utils.visualize(
                image=image_vis, 
                ground_truth_mask=gt_mask, 
                predicted_mask=pr_mask
            )
if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--dataset_path","-d", type=str, help="path where the dataset is stored")
    parser.add_argument("--input_channels","-input", type= int, default =3, help = "Input channels of images")
    parser.add_argument("--checkpoints","-ck", type=str, default = "./checkpoints_resnet34_fpn/", help= "checkpoint directory path, Path should be checkpoints_encoder_decoder")
    args = parser.parse_args()


    encoder = args.checkpoints.split("/")[0].split("_")[1]
    decoder = args.checkpoints.split("/")[0].split("_")[2]
    model = CropWeedSewia(decoder, encoder, in_channels=args.input_channels, out_classes=3)
    # evaluate model on test set
    checkpoint = torch.load(args.checkpoints, map_location = lambda storage, loc : storage)
    model.load_state_dict(checkpoint['state_dict'])
    visualize_labels(args.dataset_path, "test",model=model,device ="cuda")