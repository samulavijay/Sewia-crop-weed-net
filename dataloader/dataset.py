import os 
import numpy as np 
import cv2 
import matplotlib.pyplot as plt 


from torch.utils.data import Dataset as BaseDataset 
import torchvision.transforms as transforms
from PIL import Image
DATASET_DIR = ""

EXTENSIONS = [".jpg", ".jpeg", ".png"]
SCALES = [1.0, 0.75, 0.5]

class ToLabel:
    def __call__(self, image):
        return torch.from_numpy(np.array(image)).long()

def load_image(file):
    return Image.open(file)

def is_image(filename):
    return any(filename.endswith(ext) for ext in EXTENSIONS)

class Dataset(BaseDataset):
    def __init__(self, root, subset,  crop_h = None, crop_w = None,augmentation= None, preprocessing = None):
        self.images_root = os.path.join(root, subset, "img")
        self.labels_root = os.path.join(root, subset, "lbl")

        self.subset = subset
        assert self.subset == "train" or self.subset == "valid" or self.subset == "test"

        self.augmentation = augmentation
        self.preprocessing = preprocessing
        self.crop_h = crop_h
        self.crop_w = crop_w
        
        
        print("Images from: ", self.images_root)
        print("labels from: ", self.labels_root)

        self.filenames = [os.path.join(dp,f) for dp, dn, fn in os.walk(os.path.expanduser(self.images_root)) for f in fn if is_image(f)]
        self.filenames.sort()

        self.filenamesLabels = [os.path.join(dp,f) for dp, dn, fn in os.walk(os.path.expanduser(self.labels_root)) for f in fn if 
        is_image(f)]
        self.filenamesLabels.sort()

        assert len(self.filenames) == len(self.filenamesLabels)
    
    def __len__(self):
        return len(self.filenames)

    def __getitem__(self,index):
        image_filename = self.filenames[index]
        label_filename = self.filenamesLabels[index]

        image = cv2.imread(image_filename,cv2.IMREAD_UNCHANGED)
        label = cv2.imread(label_filename,0)
        image = cv2.resize(image,(self.crop_h,self.crop_w),interpolation=cv2.INTER_LINEAR)
        label = cv2.resize(label,(self.crop_h,self.crop_w),interpolation=cv2.INTER_AREA)
        if self.augmentation:
            augmented = self.augmentation(image= image, mask = label)
            image, label = augmented["image"], augmented["mask"]
        
        if self.preprocessing:
            preprocessed = self.preprocessing(image= image, mask = label)
            image, label = preprocessed["image"], preprocessed["mask"]

        return image, label