import torch 
import numpy as np 
import segmentation_models_pytorch as smp 
from dataloader.dataset import Dataset 
from torch.utils.data import DataLoader
import utils 
from train import CropWeedSewia
import pytorch_lightning as pl
import cv2 
import argparse

def test_images(dataset_path,checkpoints,input_channels):
    test_dataset = Dataset( root=dataset_path, subset="test", crop_h = 640, crop_w = 480,augmentation= utils.get_augmentation("test"),preprocessing = utils.get_preprocessing(preprocessing_fn))
    test_dataloader = DataLoader(test_dataset,batch_size=1, shuffle=False, num_workers=4)
    encoder = checkpoints.split("/")[0].split("_")[1]
    decoder = checkpoints.split("/")[0].split("_")[2]
    model = CropWeedSewia(decoder, encoder, in_channels=input_channels, out_classes=3)
    # evaluate model on test set
    checkpoint = torch.load(checkpoints, map_location = lambda storage, loc : storage)
    model.load_state_dict(checkpoint['state_dict'])

    trainer = pl.Trainer()
    #trainer.test(model,dataloaders=test_dataloader)

    predictions= trainer.predict(model,dataloaders=test_dataloader)
    print(predictions[0].shape,len(predictions))

    for i,prediction in enumerate(predictions):
        prediction = prediction.squeeze()
        # prediction = prediction.transpose(2,1,0)
        #print(prediction.shape)
        cv2.imwrite(f"./predictions/{i}.png",prediction)

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--dataset_path","-d", type=str, help="path where the dataset is stored")
    parser.add_argument("--input_channels","-input", type= int, default =3, help = "Input channels of images")
    parser.add_argument("--checkpoints","-ck", type=str, default = "./checkpoints_resnet34_fpn/", help= "checkpoint directory path, Path should be checkpoints_encoder_decoder")
    args = parser.parse_args()

    ENCODER = "se_resnext50_32x4d"
    ENCODER_WEIGHTS = "imagenet"
    preprocessing_fn = smp.encoders.get_preprocessing_fn(ENCODER, ENCODER_WEIGHTS)
    # create test dataset

    test_images(dataset_path=args.dataset_path,checkpoints=args.checkpoints,input_channels=args.input_channels)