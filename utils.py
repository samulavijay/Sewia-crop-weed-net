import os
import cv2
import matplotlib
matplotlib.use('TkAgg')
import matplotlib.pyplot as plt
import numpy as np
import albumentations as albu

def visualize(**images):
    """Plot images in a row"""
    n = len(images)
    plt.figure(figsize=(16,5))
    for i, (name, image) in enumerate(images.items()):
        plt.subplot(1, n, i +1)
        plt.xticks([])
        plt.yticks([])
        plt.title(''.join(name.split('_')).title())
        plt.imshow(image)
    plt.show()

def get_augmentation(subset=None):
    if subset=="train":
        train_transform = [

            albu.HorizontalFlip(p=0.5),

            albu.ShiftScaleRotate(scale_limit=0.5, rotate_limit=0, shift_limit=0.1, p=1, border_mode=0),

            albu.PadIfNeeded(min_height=320, min_width=320, always_apply=True, border_mode=0),
            albu.RandomCrop(height=320, width=320, always_apply=True),

            albu.IAAAdditiveGaussianNoise(p=0.2),
            albu.IAAPerspective(p=0.5),

            albu.OneOf(
                [
                    albu.CLAHE(p=1),
                    albu.RandomBrightness(p=1),
                    albu.RandomGamma(p=1),
                ],
                p=0.9,
            ),

            albu.OneOf(
                [
                    albu.IAASharpen(p=1),
                    albu.Blur(blur_limit=3, p=1),
                    albu.MotionBlur(blur_limit=3, p=1),
                ],
                p=0.9,
            ),

            albu.OneOf(
                [
                    albu.RandomContrast(p=1),
                    albu.HueSaturationValue(p=1),
                ],
                p=0.9,
            ),
        ]
        return albu.Compose(train_transform)
    elif subset=="valid" or subset=="test":
        """Add paddings to make image shape divisible by 32"""
        test_transform = [
            albu.PadIfNeeded(384, 480)
        ]
        return albu.Compose(test_transform)


def to_tensor(x, **kwargs):
    return x.transpose(2, 0, 1).astype('float32')


def get_preprocessing(preprocessing_fn):
    """Construct preprocessing transform
    
    Args:
        preprocessing_fn (callbale): data normalization function 
            (can be specific for each pretrained neural network)
    Return:
        transform: albumentations.Compose
    
    """
    
    _transform = [
        albu.Lambda(image=preprocessing_fn),
        albu.Lambda(image=to_tensor),
    ]
    return albu.Compose(_transform)

def color_conversion_labels(label):
    if 1 in np.unique(label):
        label_mask = np.all(label==1,axis=-1)
        label[label_mask]=255
    if 2 in np.unique(label):
        label_mask = np.all(label==2,axis=-1)
        label[label_mask]=128
    return label